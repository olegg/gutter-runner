var chai = require('chai');
var Matcher = require('../lib/matcher');
var Router = require('../lib/router');

describe('Matcher ', function () {
	it('should exist', function () {
		chai.expect(Matcher).to.be.an('object');
	});
	it('should have match method', function () {
		chai.expect(Matcher.match).to.be.a('function');
	});

	describe('Positive matches section', function () {
		describe('Simple strings matching', function () {
			it('should match equal strings', function () {
				chai.expect(Matcher.match('test', 'test')).to.equal(true);
			});
		});
		describe('Named path parameters matching', function () {
			var result = Matcher.match('test', '[:id]');
			it('Matcher.match() should return an object if second argument is a route variable', function () {
				chai.expect(result).to.be.an('object');
			});
			it('Object should contain a property with name "id" and value "test"', function () {
				chai.expect(result.id).to.be.equal('test');
			});
		});
		describe('Named path parameters matching (with regex)', function () {
			var result = Matcher.match('test', '[:id:.*]');
			it('Matcher.match() should return an object if second argument is a route variable', function () {
				chai.expect(result).to.be.an('object');
			});
			it('Object should contain a property with name "id" and value "test"', function () {
				chai.expect(result.id).to.be.equal('test');
			});
		});
	});
	describe('Negative matches section', function () {
		describe('Simple strings matching', function () {
			it('Matcher.match() should return false if strings are not equal', function () {
				chai.expect(Matcher.match('fail-test', 'test')).to.equal(false);
			});
		});
		describe('Named path parameters matching (with regex)', function () {
			it('Matcher.match() should return false if first argument do not match regex', function () {
				var result = Matcher.match('fail-test', '[:id:\\d]');
				chai.expect(result).to.be.equal(false);
			});
			it('but must accept numbers from 0 to 9999', function () { 
				var result = Matcher.match('1334', '[:id:^\\d{1,4}$]');
				chai.expect(result.id).to.be.equal('1334');
			});
		});
	});
});


describe('Router ', function () {
	var struct = {
		name: '',
		resolver: 'RootResolver',
		actions: {
			get: 'getInfo'
		},
		subdoc: [
			{
				name: 'tests',
				resolver: 'TestsResolver',
				actions: {
					get: 'getAllTests',
					post: 'addNewTest',
					delete: 'removeAllTests',
				},
				subdoc: [
					{
						name: 'groups',
						resolver: 'TestsResolver',
						actions: {
							get: 'getAllTestGroups',
							post: 'addTestGroup',
							delete: 'deleteAllTestGroups'
						},
						subdoc: [
							{
								name: '[:groupName]',
								resolver: 'TestsResolver',
								actions: {
									get: 'getTestGroup',
									put: 'editTestGroup',
									delete: 'deleteTestGroup'
								}
							}
						]
					}, {
						name: '[:testId:^\\d+$]',
						resolver: 'TestsResolver',
						actions: {
							get: 'getTest',
							put: 'editTest',
							delete: 'removeTest'
						}
					}
				]
			}
		]
	};

	var router = new Router(struct);
	it('should exist', function () {
		chai.expect(router).to.be.an('object');
	});
	it('should have route method', function () {
		chai.expect(router.route).to.be.a('function');
	});

	describe('Router.route() Positive section', function () {
		it('should resolve / route', function () {
			var result = router.route({url:'/',method: 'get'});
			result.then (function(result) {
				chai.expect(result).to.be.an('object');
				chai.expect(result.resolver).to.be.a('string');
				chai.expect(result.resolver).to.be.equal('RootResolver');
				chai.expect(result.action).to.be.a('string');
				chai.expect(result.action).to.be.equal('getInfo');
				chai.expect(result.params).to.be.an('object');
				chai.expect(result.params.length).to.be.empty;
			});
		});
		it('should resolve /tests/ route', function () {
			var result = router.route('/tests/', 'get');
			result.then (function(result) {
				chai.expect(result).to.be.an('object');
				chai.expect(result.resolver).to.be.a('string');
				chai.expect(result.resolver).to.be.equal('TestsResolver');
				chai.expect(result.action).to.be.a('string');
				chai.expect(result.action).to.be.equal('getAllTests');
				chai.expect(result.params).to.be.an('object');
				chai.expect(result.params.length).to.be.empty;
			});
		});
		it('should resolve /tests/{number} route', function () {
			var result = router.route('/tests/123', 'get');
			result.then (function(result) {
				chai.expect(result).to.be.an('object');
				chai.expect(result.resolver).to.be.a('string');
				chai.expect(result.resolver).to.be.equal('TestsResolver');
				chai.expect(result.action).to.be.a('string');
				chai.expect(result.action).to.be.equal('getTest');
				chai.expect(result.params).to.be.an('object');
				chai.expect(result.params.testId).to.be.equal('123');
			});
		});
		it('should resolve /tests/groups route', function () {
			var result = router.route('/tests/groups', 'get');
			result.then (function(result) {
				chai.expect(result).to.be.an('object');
				chai.expect(result.resolver).to.be.a('string');
				chai.expect(result.resolver).to.be.equal('TestsResolver');
				chai.expect(result.action).to.be.a('string');
				chai.expect(result.action).to.be.equal('getAllTestGroups');
				chai.expect(result.params).to.be.an('Object');
				chai.expect(result.params.length).to.be.empty;
			});
		});
		it('should resolve /tests/groups/{name} route', function () {
			var result = router.route('/tests/groups/simple', 'get');
			result.then (function(result) {
				chai.expect(result).to.be.an('object');
				chai.expect(result.resolver).to.be.a('string');
				chai.expect(result.resolver).to.be.equal('TestsResolver');
				chai.expect(result.action).to.be.a('string');
				chai.expect(result.action).to.be.equal('getTestGroup');
				chai.expect(result.params).to.be.an('object');
				chai.expect(result.params.groupName).to.be.equal('simple');
			});	
		});
	});
	describe('Router.route() Negative section', function () {
		it('should throw error when inappropriate method used', function () {
			var result = router.route('/', 'post');
			result.catch (function(result) {
				chai.expect(result).to.be.an('object');
				chai.expect(result.code).to.be.equal(405);
			});
		});
		it('should not resolve invalid routes', function () {
			var result = router.route('/testsssssss/', 'get');
			result.catch (function(result) {
				chai.expect(result).to.be.an('object');
				chai.expect(result.code).to.be.equal(404);
			});
		});
		it('should not resolve routes when url is too long', function () {
			var result = router.route({url: '/tests/123/123/123',method: 'get'});
			result.catch (function(result) {
				chai.expect(result).to.be.an('object');
				chai.expect(result.code).to.be.equal(404);
			});
		});
	});
});