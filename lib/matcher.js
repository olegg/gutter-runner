var Matcher = {
	match: function (name, candidate) {
		// three cases :
		if (candidate.indexOf('[:') === 0) {
			//candidate is a variable
			var param = {}; // return parameter instead of true
			var pos = candidate.indexOf(':', 2);
			if (pos > -1) { //we have a regex to match
				//getting regex
				var regex = RegExp(candidate.substring(pos + 1, candidate.indexOf(']')));
				if (name.match(regex)) {
					param[candidate.substring(2, pos)] = name;
				} else {
					// doesn't match expression
					return false;
				}
			} else {
				//just put it into the variable
				param[candidate.substring(2, candidate.indexOf(']'))] = name;
			}

			return param;
		} else {
			//candidate is a name
			if (name === candidate) {
				return true;
			}
		}
		return false;
	}
}

module.exports = Matcher;