var Matcher = require('./matcher');
var URL = require('url');

var Router = function (structure) {
	this.structure = structure;

	this.route = function (data, callback) {
		var params = {};
		var result = new Promise (function (resolve, reject ) {
			var parsedURL = URL.parse(data.url);
			var pathname = parsedURL.pathname;
			var query = parsedURL.query;
			if (pathname[0] === '/') { pathname = pathname.slice(1) };
			if (pathname[pathname.length - 1] === '/') { pathname = pathname.slice(0, -1) };
		
			if (query) {
				params = query.split('&').reduce(function (acc, curr) {
					var params = curr.split('=');
					acc[params[0]] = params[1];
					return acc;
				}, {});
			}

			var currentDoc = this.structure;
		
			if (!(pathname === '')) { //if parsedURL is an empty string the root is called
				var pathArr = pathname.split('/');

				for (var currentPathI = 0, maxI = pathArr.length; currentPathI < maxI; currentPathI += 1) {
					if (currentDoc.subdoc && currentDoc.subdoc.length) {
						for (var j = 0, maxJ = currentDoc.subdoc.length; j < maxJ; j += 1) {
							var possibleRoute, possibleParams;
						
							var match = Matcher.match(pathArr[currentPathI], currentDoc.subdoc[j].name)
							if (match) {
								if (typeof match === 'boolean') {
									//solid hit. we need just to proceed
									currentDoc = currentDoc.subdoc[j];
									break;
								} else {
									// parameter returned. store it as possible route and continue
									possibleRoute = currentDoc.subdoc[j];
									possibleParams = match;
								};
							}

							if (j === (maxJ - 1)) {
								if (possibleRoute) {
									//reached the end and found only parameterized subdoc
									//accept it as a valid option and continue
									currentDoc = possibleRoute;
									params = Object.assign(params, possibleParams);
								} else {
									//got error: no suitable subdocuments in this route
									reject ({code: 404, params:{}});
								};
							}
						};
					} else {
						//got error: there are no subdocuments in this route
						reject ({code: 404, params:{}});
					};
				};
			};
		
			// check methods 
			let method = data.method.toLowerCase();
			if (method === 'head' && !currentDoc.actions['head']) {
				method = 'get';
			}
			if (!currentDoc.actions[method]) {
				reject ({code: 405, params:{}});
				//TODO: Return accepted methods as a params
			};

			resolve ({ 
				resolver: currentDoc.resolver, 
				action: currentDoc.actions[method], 
				params: params 
			});
		}.bind(this));
		return result;
	};	
};

module.exports = Router;